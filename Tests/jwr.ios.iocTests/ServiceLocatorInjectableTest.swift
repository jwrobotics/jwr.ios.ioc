//
//  ServiceLocatorTest.swift
//  VikingOneTests
//
//  Created by Jens Willy Johannsen on 21/05/2021.
//

/*
Note: It is _not_ possible to test the case where a given type is _not_ registered when using `@Injectable`.
This is because that will result in a `fatalError()` which cannot be caught in the XCTeset framework.
*/

import XCTest
@testable import jwr_ios_ioc

class ServiceLocatorTest: XCTestCase {

    override func setUpWithError() throws {
		ServiceLocator.shared.registerSingleton( MainHandler.shared as Handler )
		ServiceLocator.shared.registerFactory {
			// Create new instance
			return MainProvider() as Provider
		}
    }

	override class func tearDown() {
		ServiceLocator.shared.deleteAllServices()
	}

    func testSingleton() throws {
		SingletonTester().runTest()
    }

	func testFactoryClosure() throws {
		FactoryClosureTester().runTest()
	}
}

// MARK: - Test classes

// These classes are required because the @Injectable var's initialization must happen _after_ the service location registrations which happen in the test case's setup function.

struct SingletonTester {
	@Injectable var handler: Handler

	/// Perform the actual test in here
	func runTest() {
		let result = handler.handle()
		XCTAssert( result )
	}
}

struct FactoryClosureTester {
	@Injectable var provider: Provider

	/// Perform the actual test in here
	func runTest() {
		let result = provider.provide()
		XCTAssert( result )
	}
}

// MARK: - Protocol and implementation for singleton test

public protocol Handler {
	func handle() -> Bool
}

public struct MainHandler: Handler {
	static let shared = MainHandler()
	private init() {}

	public func handle() -> Bool {
		return true
	}
}

// MARK: - Protocol and implementation for factory closure test

public protocol Provider {
	func provide() -> Bool
}

public struct MainProvider: Provider {
	public func provide() -> Bool {
		return true
	}
}
