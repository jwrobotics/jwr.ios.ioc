//
//  ServiceLocatorInjectableOptionalTests.swift
//  VikingOneTests
//
//  Created by Jens Willy Johannsen on 25/05/2021.
//

import XCTest
@testable import jwr_ios_ioc

class ServiceLocatorInjectableOptionalTests: XCTestCase {
    override func setUpWithError() throws {
        // No pre-test setup
    }

	override class func tearDown() {
		ServiceLocator.shared.deleteAllServices()
	}

	func testOptionalSingletonNil() {
		// At this time, there is no singleton registered for `Handler`, so we expect a nil value
		let tester = OptionalSingletonTester()
		XCTAssertNil( tester.runTest() )
	}

	func testOptionalSingletonNotNil() {
		// Register a singleton first, then instantiate and verify
		ServiceLocator.shared.registerSingleton( MainHandler.shared as Handler )

		let tester = OptionalSingletonTester()	// At this point, there _is_ regsitered a singleton for `Handler`, so the `.runTest()` should result in a non-nil value
		XCTAssertNotNil( tester.runTest() )

		XCTAssert( tester.runTest() ?? false )	// Use `?? false` so the assertion will fail if `nil` is returned
	}

	func testOptionalFactoryNil() {
		// At this time, there is no singleton registered for `Provider`, so we expect a nil value
		let tester = OptionalFactoryClosureTester()
		XCTAssertNil( tester.runTest() )
	}

	func testOptionalFactoryNotNil() {
		// Register a singleton first, then instantiate and verify
		ServiceLocator.shared.registerFactory {
			// Return a new instance every time
			return MainProvider() as Provider
		}

		let tester = OptionalFactoryClosureTester()	// At this point, there _is_ regsitered a singleton for `Provider`, so the `.runTest()` should result in a non-nil value
		XCTAssertNotNil( tester.runTest() )

		XCTAssert( tester.runTest() ?? false )	// Use `?? false` so the assertion will fail if `nil` is returned
	}

}

// MARK: - Test classes

// These classes are required because the @Injectable var's initialization must happen _after_ the service location registrations which happen in the test case's setup function.

struct OptionalSingletonTester {
	@InjectableOptional var handler: Handler?

	/// Perform the actual test in here
	func runTest() -> Bool? {
		return handler?.handle()
	}
}

struct OptionalFactoryClosureTester {
	@InjectableOptional var provider: Provider?

	/// Perform the actual test in here
	func runTest() -> Bool? {
		return provider?.provide()
	}
}
