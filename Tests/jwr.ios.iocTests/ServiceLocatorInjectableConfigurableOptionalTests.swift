//
//  ServiceLocatorInjectableOptionalTests.swift
//  VikingOneTests
//
//  Created by Jens Willy Johannsen on 25/05/2021.
//

import XCTest
@testable import jwr_ios_ioc

/// Static configuration instances so the same instances can be re-used
fileprivate class Configurations {
	static let config1 = Configuration(name: "config1", id: 1)
	static let config2 = Configuration(name: "config2", id: 2)
}

class ServiceLocatorInjectableConfigurableOptionalTests: XCTestCase {
    override func setUpWithError() throws {
        // No pre-test setup
    }

	override class func tearDown() {
		ServiceLocator.shared.deleteAllServices()
	}

	// MARK: Singleton tests

	func testOptionalSingletonNil() {
		// At this time, there is no singleton registered for `Handler` regardless of configuration so we expect a nil value for all tests
		let tester = ConfigurableOptionalSingletonTester()
		XCTAssertNil( tester.runTest1() )
		XCTAssertNil( tester.runTest2() )
		XCTAssertNil( tester.runTest3() )
	}

	func testOptionalSingletonNotNilConfig1() {
		// Register a singleton for Configurations.config1, then instantiate and verify
		ServiceLocator.shared.registerConfigurableSingleton(configuration: Configurations.config1, singleton: MainHandler.shared as Handler)

		let tester = ConfigurableOptionalSingletonTester()	// At this point, there _is_ registered a singleton for `Handler/Configurations.config1`, so the `.runTest1()` should result in a non-nil value
		XCTAssertNotNil( tester.runTest1() )
		XCTAssert( tester.runTest1() ?? false )	// Use `?? false` so the assertion will fail if `nil` is returned
	}

	func testOptionalSingletonNilConfig2() {
		// Register a singleton for Configurations.config1, then instantiate and verify
		ServiceLocator.shared.registerConfigurableSingleton(configuration: Configurations.config1, singleton: MainHandler.shared as Handler)

		let tester = ConfigurableOptionalSingletonTester()	// There is only registered a service for `Handler/Configurations.config1` so `.runTest2()` should still return nil
		XCTAssertNil( tester.runTest2() )
	}

	func testOptionalSingletonNotNilOtherInstance() {
		// Register a singleton for Configurations.config1, then instantiate and verify
		ServiceLocator.shared.registerConfigurableSingleton(configuration: Configurations.config1, singleton: MainHandler.shared as Handler)

		let tester = ConfigurableOptionalSingletonTester()	// A service is registered for `Handler/Configurations.config1` so even though `runTest3()` uses a different configuration _instance_, the _hashes_ has the same so non-nil should be returned.
		XCTAssertNotNil( tester.runTest3() )
		XCTAssert( tester.runTest3() ?? false )	// Use `?? false` so the assertion will fail if `nil` is returned
	}

	// MARK: Factory tests

	func testOptionalFactoryNil() {
		// At this time, there is no singleton registered for `Provider`, so we expect a nil value for all tests
		let tester = ConfigurableOptionalFactoryClosureTester()
		XCTAssertNil( tester.runTest1() )
		XCTAssertNil( tester.runTest2() )
		XCTAssertNil( tester.runTest3() )
	}

	func testOptionalFactoryNotNilConfig1() {
		// Register a factory for Configurations.config1, then instantiate and verify
		ServiceLocator.shared.registerConfigurableFactory(configuration: Configurations.config1) {
			return MainProvider() as Provider
		}

		let tester = ConfigurableOptionalFactoryClosureTester()	// At this point, there _is_ registered a factory for `Handler/Configurations.config1`, so the `.runTest1()` should result in a non-nil value
		XCTAssertNotNil( tester.runTest1() )
		XCTAssert( tester.runTest1() ?? false )	// Use `?? false` so the assertion will fail if `nil` is returned
	}

	func testOptionalFactoryNilConfig2() {
		// Register a factory for Configurations.config1, then instantiate and verify
		ServiceLocator.shared.registerConfigurableFactory(configuration: Configurations.config1) {
			return MainProvider() as Provider
		}

		let tester = ConfigurableOptionalFactoryClosureTester()	// There is only registered a service for `Handler/Configurations.config1` so `.runTest2()` should still return nil
		XCTAssertNil( tester.runTest2() )
	}

	func testOptionalFactoryNotNilOtherInstance() {
		// Register a factory for Configurations.config1, then instantiate and verify
		ServiceLocator.shared.registerConfigurableFactory(configuration: Configurations.config1) {
			return MainProvider() as Provider
		}

		let tester = ConfigurableOptionalFactoryClosureTester()	// A service is registered for `Handler/Configurations.config1` so even though `runTest3()` uses a different configuration _instance_, the _hashes_ has the same so non-nil should be returned.
		XCTAssertNotNil( tester.runTest3() )
		XCTAssert( tester.runTest3() ?? false )	// Use `?? false` so the assertion will fail if `nil` is returned
	}
}

// MARK: - Test classes

// These classes are required because the @Injectable var's initialization must happen _after_ the service location registrations which happen in the test case's setup function.

fileprivate struct ConfigurableOptionalSingletonTester {
	@InjectableConfigurableOptional(configuration: Configurations.config1) var handler1: Handler?
	@InjectableConfigurableOptional(configuration: Configurations.config2) var handler2: Handler?
	@InjectableConfigurableOptional(configuration: Configuration(name: "config1", id: 1)) var handler3: Handler?	// Same as Configurations.config1, different instance but same hash

	func runTest1() -> Bool? {
		return handler1?.handle()
	}

	func runTest2() -> Bool? {
		return handler2?.handle()
	}

	func runTest3() -> Bool? {
		return handler3?.handle()
	}
}

fileprivate struct ConfigurableOptionalFactoryClosureTester {
	@InjectableConfigurableOptional(configuration: Configurations.config1) var provider1: Provider?
	@InjectableConfigurableOptional(configuration: Configurations.config2) var provider2: Provider?
	@InjectableConfigurableOptional(configuration: Configuration(name: "config1", id: 1)) var provider3: Provider?	// Same as Configurations.config1, different instance but same hash

	func runTest1() -> Bool? {
		return provider1?.provide()
	}

	func runTest2() -> Bool? {
		return provider2?.provide()
	}

	func runTest3() -> Bool? {
		return provider3?.provide()
	}
}
