//
//  InjectablePropertyWrappers.swift
//
//  Created by Jens Willy Johannsen on 21/05/2021.
//

import Foundation

/**
Property wrapper to inject an instance of the specified type.

The requested type _must_ be registered in `ServiceLocator.shared` using either a singleton instance or a factory closure _before_ getting an instance using this property wrapper.

## Warning

⚠️ The app _will_ terminate with a `fatalError` if the specified type is _not_ registered when the `@Injectable` variable is instantiated!

## Usage

```
struct SomeStruct {
  @Injectable var permissionHandler: PermissionHandler

  func test() {
    permissionHandler.doStuff()
  }
}
```
*/
@propertyWrapper
public struct Injectable<T> {
	public var wrappedValue: T {
		return dependency
	}

	private let dependency: T

	public init() {
		// Locate (non-optional) service
		self.dependency = ServiceLocator.shared.locate()
	}
}

/**
Property wrapper to inject an instance of the specified type.

The requested type should be registered in `ServiceLocator.shared` using either a singleton instance or a factory closure _before_ getting an instance using this property wrapper.

If no singleton/closure is registered for the specified type, the value of the variable will be `nil`.

## Usage

```
struct SomeStruct {
  @InjectableOptional var permissionHandler: PermissionHandler?

  func test() {
    if let handler = permissionHandler {
      handler.doStuff()
    } else {
      print( "No PermissionHandler registered." )
    }
}
```
*/
@propertyWrapper
public struct InjectableOptional<T> {
	public var wrappedValue: T? {
		return dependency
	}

	private let dependency: T?

	public init() {
		// Locate optional service
		self.dependency = ServiceLocator.shared.locateOptional()
	}
}

/**
Property wrapper to inject an instance of the specified type with a specific configuration.

Note that the configuration object must be initialized and accessible _before_ the initializer for the property wrapper runs.

The requested type must be registered in `ServiceLocator.shared` using either a singleton instance or a factory closure _before_ getting an instance using this property wrapper.

## Warning

⚠️ The app _will_ terminate with a `fatalError` if the specified type/configuration is _not_ registered when the `@InjectableConfigurable` variable is instantiated!

## Usage

```
struct SomeStruct {
  @InjectableConfigurable(configuration: globals.permissionConfiguration) var permissionHandler: PermissionHandler

  func test() {
    permissionHandler.doStuff()
  }
}
```
*/
@propertyWrapper
public struct InjectableConfigurable<T, S: Hashable> {
	var configuration: S

	private let dependency: T

	public var wrappedValue: T {
		return dependency
	}

	public init( configuration: S ) {
		self.configuration = configuration
		self.dependency = ServiceLocator.shared.locateConfigurable(configuration: configuration)
	}
}

/**
Property wrapper to inject an instance of the specified type with the specified configuration instance.

The requested type should be registered in `ServiceLocator.shared` using either a singleton instance or a factory closure _before_ getting an instance using this property wrapper.

If no singleton/closure is registered for the specified type, the value of the variable will be `nil`.

## Usage

```
let configuration = "Config1"

struct SomeStruct {
	@InjectableConfigurableOptional(configuration: configuration) var permissionHandler: PermissionHandler?

	func test() {
		if let handler = permissionHandler {
			handler.doStuff()
		} else {
			print( "No PermissionHandler registered." )
	}
}
```
*/
@propertyWrapper
public struct InjectableConfigurableOptional<T, S: Hashable> {
	var configuration: S

	private let dependency: T?

	public var wrappedValue: T? {
		return dependency
	}

	public init( configuration: S ) {
		self.configuration = configuration
		self.dependency = ServiceLocator.shared.locateOptionalConfigurable(configuration: configuration)
	}
}
