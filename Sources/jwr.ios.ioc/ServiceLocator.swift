//
//  ServiceLocator.swift
//
//  Created by Jens Willy Johannsen on 21/05/2021.
//

import Foundation

/// Service Locator class for handling inversion-of-control. Be sure to register either a singleton or a "factory closure" for each type that should be locatable.
public final class ServiceLocator {
	/// Singleton instance
	public static let shared = ServiceLocator()
	private init() {}

	/// Registry dictionary storing singleton/factory closure for each registered type.
	private var registry = [ObjectIdentifier: Any]()

	/// Registry dictionary storing singleton/factory closures for configurable types. A `ConfigurationAndType` instance containing object identifier for the type and hash for the configuration is used as key.
	private var configurableRegistry = [ConfigurationAndType: Any]()

	// MARK: - Service Registration

	/**
	Register a service using a factory closure

	- parameter factory: A closure returning an instance of the requested type.
	*/
	public func registerFactory<T>(_ factory: @escaping (() -> T) ) {
		let typeID = ObjectIdentifier( T.self )

		// Register type factory closure with type identifier as a key.
		self.registry[typeID] = factory
	}

	/**
	Register a service using a singleton instance.

	- parameter singleton: Object to use as singleton for the requested type.
	*/
	public func registerSingleton<T>(_ singleton: T ) {
		let typeID = ObjectIdentifier( T.self )

		// Register type instance with type identifier as a key.
		self.registry[typeID] = singleton
	}

	// MARK: - Service Injection

	/**
	Get an instance of the requested type.

	**NB:** A `fatalError` will happen if the requested type is not registered.

	- returns: An instance of the requested type
	*/
	public func locate<T>() -> T {
		guard let service: T = locateService() else {
			// Unable to locate singleton/factory closure for requested type: fatal error
			fatalError( "Service locator: No registered instance for type \(T.self)" )
		}

		return service
	}

	/**
	Get an instance of the requested type.

	This is the "nil-safe" version that will return `nil` if the requested type is not registered.

	- returns: An instance of the requested type or `nil` if no service is registered for the requested type.
	*/
	public func locateOptional<T>() -> T? {
		return locateService()
	}

	/// Private function to find either singleton instance or call factory closure to get instance to return. `nil` is returned if neither is registered for the requested (inferred) type.
	private func locateService<T>() -> T? {
		let typeID = ObjectIdentifier( T.self )

		// First look for a factory closure of the requested type, if that fails look for a singleton instance. If neither are found, return `nil`.
		if let factoryClosure = registry[typeID] as? (() -> T) {
			// Call closure and return result
			return factoryClosure()
		} else if let singletonInstance = registry[typeID] as? T {
			return singletonInstance
		}

		// Fall-through: didn't find anything
		return nil
	}

	/**
	Removes all registered services.
	*/
	public func deleteAllServices() {
		registry.removeAll()
		configurableRegistry.removeAll()
	}
}

/**
These functions allows for registering types with an associated configuration.

Both singletons and factories can be registered and multiple singleton/factories can be registered for the same type but with different configurations.

The configuration must be an instance of a hashable type.

When retrieving (locating) the service for the type/configuation, the configuration's `hashValue` is used as key.

It is possible to locate a service using `@InjectableConfigurable(configuation:)`, but the configuration must already be instantiated then the property wrapper initializer is run.
*/
extension ServiceLocator {
	/**
	Register a singleton for the specified type with a configuration instance.

	- parameter configuration: A `Hashable` configuration instance
	- parameter singleton: The singleton to use for this type/configuration
	*/
	public func registerConfigurableSingleton<T, S: Hashable>( configuration: S, singleton: T ) {
		let typeID = ConfigurationAndType(typeIdentifier: ObjectIdentifier(T.self), configurationHash: configuration.hashValue)

		self.configurableRegistry[typeID] = singleton
	}

	/**
	Register a factory closure for the specified type with a configuration instance.

	- parameter configuration: A `Hashable` configuration instance
	- parameter factory: A closure that returns an instance of the type
	*/
	public func registerConfigurableFactory<T, S: Hashable>( configuration: S, factory: @escaping (() -> T) ) {
		let typeID = ConfigurationAndType(typeIdentifier: ObjectIdentifier(T.self), configurationHash: configuration.hashValue)

		self.configurableRegistry[typeID] = factory
	}


	/**
	Get an instance of the requested type for the requested configuration.

	**NB:** A `fatalError` will happen if the requested type/configuration combination is not registered.

	- parameter configuration: A `Hashable` configuration instance
	- returns: An instance of the requested type
	*/
	public func locateConfigurable<T, S: Hashable>(configuration: S) -> T {
		guard let service: T = locateConfigurableService(configuration: configuration) else {
			fatalError( "Not found" )
		}

		return service
	}

	/**
	Get an instance of the requested type for the requested configuration.

	This is the "nil-safe" version that will return `nil` if the requested type/configuration combination is not registered.

	- parameter configuration: A `Hashable` configuration instance
	- returns: An instance of the requested type or `nil` if no service is registered for the requested type/configuration.
	*/
	public func locateOptionalConfigurable<T, S: Hashable>(configuration: S) -> T? {
		return locateConfigurableService(configuration: configuration)
	}

	/// Private function to find either singleton instance or call factory closure for a configurable type to get instance to return. `nil` is returned if neither is registered for the requested (inferred) type.
	private func locateConfigurableService<T, S: Hashable>(configuration: S) -> T? {
		let typeID = ConfigurationAndType(typeIdentifier: ObjectIdentifier(T.self), configurationHash: configuration.hashValue)

		// First look for a factory closure of the requested type/configuration, if that fails look for a singleton instance. If neither are found, return `nil`.
		if let factoryClosure = configurableRegistry[typeID] as? (() -> T) {
			// Call closure and return result
			return factoryClosure()
		} else if let singletonInstance = configurableRegistry[typeID] as? T {
			return singletonInstance
		}

		// Fall-through: didn't find anything
		return nil
	}
}

/// Struct containing an object identifier and a hash. The hash of this struct is used as key for configuration-based singletons/factories
struct ConfigurationAndType: Hashable {
	let typeIdentifier: ObjectIdentifier
	let configurationHash: Int
}

